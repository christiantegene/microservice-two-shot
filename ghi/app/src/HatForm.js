import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

const HatForm = () => {
  const navigate = useNavigate();
  const [submitted, setSubmitted] = useState(false);

  const [locations, setLocations] = useState([]);

  const [name, setName] = useState('');
  const [fabric, setFabric] = useState('');
  const [style_name, setStyle_name] = useState('');
  const [color, setColor] = useState('');
  const [picture_url, setPicture_url] = useState('');
  const [location, setLocation] = useState('');
  const [selectedLocation, setSelectedLocation] = useState([])

  const fetchData = async () => {
      const url = "http://localhost:8100/api/locations";
      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
      }
  }

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const handleStyle_nameChange = (event) => {
    const value = event.target.value;
    setStyle_name(value);
  }

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handlePicture_urlChange = (event) => {
    const value = event.target.value;
    setPicture_url(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data ={}

    data.name = name;
    data.fabric = fabric;
    data.style_name = style_name;
    data.color = color;
    data.picture_url = picture_url;
    data.location = location;

    const hatUrl = `http://localhost:8090/api/hats/`
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {

      setName('');
      setFabric('');
      setStyle_name('');
      setColor('');
      setPicture_url('');
      setLocation('');


    }
    navigate('/hats');
  }

  useEffect(() => {
      fetchData();
  }, [])

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Hat name" required type="text" name="name" id="name" className="form-control" value={name} />
                <label htmlFor="name">Hat name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyle_nameChange} placeholder="Style name" type="text" name="style_name" id="style_name" className="form-control" value={style_name} />
                <label htmlFor="style_name">Style name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                <label htmlFor="color">color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture_urlChange} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control" value={picture_url} />
                <label htmlFor="color">picture url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location} >
                  <option value="">Choose a location</option>
                  {locations.map((location) => {
                    return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HatForm
