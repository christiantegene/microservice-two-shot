import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"


const ShoeList = () => {

  const [shoes, setShoes] = useState([])
  const fetchData = async () => {

    const url = 'http://localhost:8080/api/shoes/';


      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes)
      }
    }


    const removeShoe = async (event, id) => {
      event.preventDefault();
      const url = `http://localhost:8080/api/shoes/${id}`;
      const response = await fetch(url, {method: "DELETE" });
      if (response.ok) {
        setShoes(shoes.filter((shoes) => shoes.id !== id));
      }
    };


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <h1>All the best shoes</h1>
      <Link to="new" className="btn btn-lg btn-primary">
          Add a new pair
      </Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Model name</th>
            <th>Color</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
          return (
          <tr key={shoe.id} value={shoe.id}>
            <td>{shoe.name}</td>
            <td>{shoe.manufacturer}</td>
            <td>{shoe.model_name}</td>
            <td>{shoe.color}</td>
            <td><img src={shoe.picture_url} style={{width:"200px"}}></img></td>
            <td><button onClick={(event) => removeShoe(event, shoe.id)}>Delete</button></td>
          </tr>
          );
        })}
        </tbody>

      </table>

    </div>

  )
}

export default ShoeList
