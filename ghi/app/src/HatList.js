import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom"

const HatList = () => {
  const [hats, setHats] = useState([]);

  const fetchData = async ()=> {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  };

  const deleteHat = async (event, id) => {
    event.preventDefault();
    const url = `http://localhost:8090/api/hats/${id}`;
    const response = await fetch(url, {method: "DELETE" });
    if (response.ok) {
      setHats(hats.filter((hat) => hat.id !== id));
    }
  };

  const addHat =

  useEffect(() => {
    fetchData();
  }, [])

  return (
    <div className="container">
      <h1>All the best hats</h1>
      <Link to="new" className="btn btn-lg btn-primary">
          Add a new hat
      </Link>
      <table className="table table-striped">
          <thead>
          <tr>
              <th>Name</th>
              <th>Fabric</th>
              <th>Style name</th>
              <th>Color</th>
          </tr>
          </thead>
          <tbody>
          {hats.map(hat => {
              return (
              <tr key={hat.id} value={hat.id} >
                  <td>{hat.name}</td>
                  <td>{hat.fabric}</td>
                  <td>{hat.style_name}</td>
                  <td>{hat.color}</td>
                  <td><img src={hat.picture_url} style={{ width:"200px" }} /></td>
                  <td>
                    <button className="btn btn-lg btn-outline-primary" onClick={(event) => deleteHat(event, hat.id)}>Delete</button>
                  </td>
              </tr>
              );
          })}
          </tbody>
      </table>
    </div>
  )
}

export default HatList
