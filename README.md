# Wardrobify

Team:

* Christian - Shoes
* Dara Karbassioon - Hats

## Design
We'll use Bootstrap.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I plan on creating a Shoe Model as well as a ShoeVO model. The ShoeVO model will poll the Wardrobe APi for updates.

## Hats microservice

I will have a hat model and a hatVO model which will poll to the wardrobe API.
