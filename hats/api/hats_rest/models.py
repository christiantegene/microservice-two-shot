from django.db import models
from django.urls import reverse

# Create your models here.

class HatVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Hats(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        HatVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_hats_detail", kwargs={"id": self.id})
