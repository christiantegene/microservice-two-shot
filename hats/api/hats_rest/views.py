from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hats, HatVO

class HatVOEncoder(ModelEncoder):
    model = HatVO
    properties = ["name", "import_href", "id"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name", "id"
    ]

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": HatVOEncoder()
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def show_hat_list(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            #make sure the location matches up
            location_href = content["location"]
            #import_href is a unique identifier like an id
            location = HatVO.objects.get(import_href=location_href)
            content["location"] = location
        except HatVO.DoesNotExist:
            return JsonResponse({
                "message" : "Location does not exist"
            }, status=400)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def show_hat_details(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
