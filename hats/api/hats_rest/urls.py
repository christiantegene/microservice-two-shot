from django.urls import path
from .views import show_hat_list, show_hat_details

urlpatterns = [
    path("hats/", show_hat_list, name="create_hats"),
    path(
        "locations/<int:hat_vo_id>/hats/",
        show_hat_list,
        name="show_hat_list",
        ),
    path("hats/<int:id>/", show_hat_details, name="show_hat_detail"),

]
