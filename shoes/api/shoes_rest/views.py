from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoes, ShoesVO


class ShoesVODetailEncoder(ModelEncoder):
    model = ShoesVO
    properties = ["name", "import_href", "id"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "id",
    ]

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": ShoesVODetailEncoder(),
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def show_shoes_list(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesDetailEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = ShoesVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except ShoesVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    else:
        count , _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
