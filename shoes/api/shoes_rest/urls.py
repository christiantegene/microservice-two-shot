from django.urls import path
from .views import show_shoes_list, show_shoe


urlpatterns = [
    path("shoes/", show_shoes_list, name="create_shoes"),
    path(
        "bins/<int:shoes_vo_id>/shoes/",
        show_shoes_list,
        name="show_shoes_list",
    ),
    path("shoes/<int:id>/", show_shoe, name="show_shoe")
]
