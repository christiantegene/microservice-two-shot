from django.contrib import admin
from .models import Shoes, ShoesVO

# Register your models here.
@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    pass

@admin.register(ShoesVO)
class ShoesVOAdmin(admin.ModelAdmin):
    pass
