from django.db import models
from django.urls import reverse


# Create your models here.

class ShoesVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Shoes(models.Model):
    name = models.CharField(max_length=200, default='')
    manufacturer = models.CharField(max_length=200, null=True)
    model_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(ShoesVO, related_name="shoes", on_delete=models.CASCADE, default='')


def __str__(self):
        return self.name


def get_api_url(self):
        return reverse("show_shoe", kwargs={"id": self.id})
